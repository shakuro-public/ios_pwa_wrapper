//
//  ViewController.swift
//
//  Created by Vlad Onipchenko on 7/14/20.
//

import UIKit
import WebKit

class WrapperViewController: UIViewController {

    private enum Constant {
        #if PRODUCTION
        static let appURL = URL(string: "https://pwa-for-twa-notifications-example.vercel.app")
        #else
        static let appURL = URL(string: "https://pwa-for-twa-notifications-example.vercel.app")
        #endif
        
        static let allowedHost = "pwa-for-twa-notifications-example.vercel.app"
    }
    
    @IBOutlet private var webViewContainer: UIView!
    @IBOutlet private var loadingView: UIView!
    @IBOutlet private var progressView: UIProgressView!
    
    private let webView: WKWebView = {
        return WrapperViewController.setupWebView()
    }()
    
    private var estimatedProgressToken: NSKeyValueObservation?
    
    private let apn = APNService.shared
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            return .default
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webView.frame = webViewContainer.bounds
        webView.navigationDelegate = self
        webView.uiDelegate = self
        webViewContainer.addSubview(webView)
        webView.layoutIfNeeded()
        
        estimatedProgressToken = webView.observe(\.estimatedProgress) { [weak self] (webView, _) in
            guard let actualSelf = self else {
                return
            }
            actualSelf.progressView.setProgress(Float(actualSelf.webView.estimatedProgress), animated: true)
        }
        apn.registerForRemoteNotifications()
        reloadWebApp()
    }
    
}

// MARK: - WKNavigationDelegate

extension WrapperViewController: WKNavigationDelegate, WKUIDelegate {
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame == nil {
            webView.load(navigationAction.request)
        }
        return nil
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let url = navigationAction.request.url else {
            decisionHandler(.allow)
            return
        }
        if let host = url.host, host.lowercased().contains(Constant.allowedHost) {
            decisionHandler(.allow)
        } else {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            decisionHandler(.cancel)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        guard !loadingView.isHidden else {
            return
        }
        estimatedProgressToken = nil
        loadingView.isHidden = true
        loadingView.layer.addTransitionAnimation(duration: 0.2)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        debugPrint("didFail navigation \(error)")
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        showErrorIfNeeded(error)
    }
    
}

// MARK: - Private

private extension WrapperViewController {
    
    static func setupWebView() -> WKWebView {
        let configuration: WKWebViewConfiguration = WKWebViewConfiguration()
        configuration.preferences.javaScriptEnabled = true
        configuration.preferences.javaScriptCanOpenWindowsAutomatically = true
        configuration.allowsInlineMediaPlayback = true
        configuration.ignoresViewportScaleLimits = false
        let webView: WKWebView = WKWebView(frame: CGRect.zero,
                                           configuration: configuration)
        webView.backgroundColor = UIColor.white
        webView.scrollView.backgroundColor = UIColor.white
        webView.scrollView.showsVerticalScrollIndicator = true
        webView.scrollView.showsHorizontalScrollIndicator = false
        webView.isExclusiveTouch = true
        return webView
    }
    
    func showErrorIfNeeded(_ error: Error) {
        // Handle first loading error only, other errors should be handled by pwa
        guard (error as NSError).code != NSURLErrorCancelled, presentedViewController == nil, !loadingView.isHidden  else {
            return
        }
        let action = UIAlertAction(title: NSLocalizedString("Retry", comment: ""),
                                   style: .default,
                                   handler: { [weak self] (_) in
                                    self?.reloadWebApp()
                                   })
        let alertController: UIAlertController = UIAlertController(title: NSLocalizedString("Oops!", comment: ""),
                                                                   message: error.localizedDescription,
                                                                   preferredStyle: .alert)
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }
    
    func reloadWebApp() {
        apn.requesFCMToken()
        apn.handleFCMToken = { [weak self] (token) in
            DispatchQueue.main.async { [weak self] in
                guard let actualSelf = self,
                      let appURL = Constant.appURL,
                      var components = URLComponents(url: appURL, resolvingAgainstBaseURL: false) else {
                    return
                }
                components.query = "_notifyToken=\(token)"
                let requestURL = components.url ?? appURL
                let urlRequest = URLRequest(url: requestURL)
                actualSelf.webView.load(urlRequest)
            }
        }
        
    }

}
