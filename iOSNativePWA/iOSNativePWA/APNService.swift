import UIKit
import UserNotifications
import FirebaseCore
import FirebaseMessaging
import Firebase
import FirebaseInstanceID

final class APNService: NSObject {

    private enum Constants {
        static let fcmTokenKey = "fcmTokenKey"
    }

    static let shared = APNService()

    var handleFCMToken: ((String) -> Void)?

    private (set) var isRegisteredForRemoteNotifications: Bool = false
    private (set) var fcmToken: String = "" {
        didSet {
            UserDefaults.standard.setValue(fcmToken, forKey: Constants.fcmTokenKey)
        }
    }

    private enum TokenState {
        case new(String, Data)
        case sended(String, Data)
        case empty

        func getToken() -> String? {
            switch self {
            case .new(let token, _), .sended(let token, _):
                return token
            case .empty:
                return nil
            }
        }
    }

    private var apnToken: TokenState = .empty

    override init() {
        super.init()
        FirebaseApp.configure()
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().delegate = self
    }

    func requesFCMToken() {
        InstanceID.instanceID().instanceID { [weak self] (result, _) in
            guard let actualResult = result else {
                return
            }
            self?.fcmToken = actualResult.token
            self?.handleFCMToken?(actualResult.token)
        }
    }

    func registerForRemoteNotifications() {
        isRegisteredForRemoteNotifications = true
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: { (_, _) in
            DispatchQueue.main.async(execute: {
                UIApplication.shared.registerForRemoteNotifications()
            })
        })
    }

    func unRegisterForRemoteNotifications() {
        isRegisteredForRemoteNotifications = false
        UIApplication.shared.unregisterForRemoteNotifications()
        apnToken = .empty
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0
        sendToken(forced: false)
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.reduce(into: "", { $0.append(String(format: "%02x", $1)) })
        apnToken = .new(token, deviceToken)
        sendToken(forced: true)
    }
}

// MARK: - MessagingDelegate

extension APNService: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        self.fcmToken = fcmToken
        handleFCMToken?(fcmToken)
    }
}

// MARK: - UNUserNotificationCenterDelegate

extension APNService: UNUserNotificationCenterDelegate {

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void) {
        defer {
            completionHandler()
        }
        handleNotification(response)
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.badge, .sound, .alert])
    }
}

// MARK: - Private

private extension APNService {

    func handleNotification(_ response: UNNotificationResponse) {
        // TODO: handle notification here
    }

    func sendToken(forced: Bool) {
        let toSendToken: Data?
        let tokenStr: String
        switch apnToken {
        case .new(let str, let token):
            toSendToken = token
            tokenStr = str
        case .sended(let str, let token):
            toSendToken = forced ? token : nil
            tokenStr = str
        case .empty:
            toSendToken = nil
            tokenStr = ""
        }
        guard let actualToken = toSendToken else {
            return
        }
        Messaging.messaging().apnsToken = actualToken
        apnToken = .sended(tokenStr, actualToken)
    }
}
