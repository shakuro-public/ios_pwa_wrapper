# iOS PWA

A native iOS  application to wrap Progressive Web App.

## License

iOS Native PWA is released under the MIT license. [See LICENSE](https://gitlab.com/shakuro-public/ios_pwa_wrapper/blob/master/LICENSE) for details.
